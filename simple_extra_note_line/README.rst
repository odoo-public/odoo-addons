.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :alt: License: AGPL-3

Simple Extra Notes
==================
Extra Notes on Sale Orders and Sale Order Lines, Propagating to Dilvery and Invoices on your choice.

Bug Tracker
===========
Bugs are tracked on `Gitlab Issues <https://gitlab.com/odoo-public/odoo-addons/-/issues>`_.

Credits
=======

Author:
* André Schenkels (ICTSTUDIO)


Maintainer
==========
.. image:: https://www.ictstudio.eu/github_logo.png
   :alt: ICTSTUDIO
   :target: https://www.ictstudio.eu

This module is maintained by the ICTSTUDIO.