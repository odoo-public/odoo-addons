# Copyright© 2016-2017 ICTSTUDIO <http://www.ictstudio.eu>
# License: LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)
{
    'name': 'Simple Extra Notes',
    'version': '16.0.0.0.1',
    'category': 'CRM',
    'author': 'ICTSTUDIO | André Schenkels',
    'website': 'http://www.ictstudio.eu',
    'license': 'LGPL-3',
    'summary': 'Extra Notes on Sale Orders and Sale Order Lines, Propagating to Dilvery and Invoices on your choice.',
    'depends': [
        'simple_extra_note',
    ],
    'data': [
        'views/account_move.xml',
        'views/sale_order.xml',
        'views/stock_picking.xml'

    ],
    'installable': False,
}