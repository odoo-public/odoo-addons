# Copyright© 2016-2017 ICTSTUDIO <http://www.ictstudio.eu>
# License: LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from . import sale_order_line
from . import stock_move
from . import account_move_line
from . import stock_rule

