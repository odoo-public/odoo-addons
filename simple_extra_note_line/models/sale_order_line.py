# Copyright© 2020 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

import logging

from odoo import models, fields

_logger = logging.getLogger(__name__)

class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    extra_note = fields.Text(
        string="Note",
    )

    def _prepare_invoice_line(self):
        values = super(SaleOrderLine, self). _prepare_invoice_line()
        if self.extra_note and values and self.order_id.extra_note_copy_to_invoice:
            values.update({'extra_note': self.extra_note})
        return values

    def _prepare_procurement_values(self, group_id=False):
        values = super(SaleOrderLine, self)._prepare_procurement_values(group_id)
        self.ensure_one()
        if self.extra_note and values and self.order_id.extra_note_copy_to_picking:
            values.update({'extra_note': self.extra_note})
        return values
