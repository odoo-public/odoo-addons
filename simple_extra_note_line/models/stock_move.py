# Copyright© 2020 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

import logging

from odoo import models, fields

_logger = logging.getLogger(__name__)

class StockMove(models.Model):
    _inherit = "stock.move"

    extra_note = fields.Text(
        string="Note"
    )
