.. image:: https://img.shields.io/badge/License-LGPL%20v3-blue.svg
    :alt: License: LGPL-3

Dropshipment Client Order Reference
====================
Dropshipment Address Object for use in Dropshipment Sale module

Bug Tracker
===========
Bugs are tracked on `GitHub Issues <https://github.com/ICTSTUDIO/odoo-extra-addons/issues>`_.

Maintainer
==========
.. image:: https://www.ictstudio.eu/github_logo.png
   :alt: ICTSTUDIO
   :target: https://www.ictstudio.eu

This module is maintained by the ICTSTUDIO.