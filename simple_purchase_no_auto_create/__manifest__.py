# -*- coding: utf-8 -*-
# Copyright© 2018-present ICTSTUDIO <http://www.ictstudio.eu>
# License: LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)
{
    'name': 'Simple Purchase No Automatic Supplierinfo',
    'version': '13.0.1.0.1',
    'category': 'Purchase Management',
    'author': 'ICTSTUDIO, André Schenkels',
    'website': 'http://www.ictstudio.eu',
    'license': 'LGPL-3',
    'summary': """Don't automatic create a supplierinfo line""",
    'depends': [
        'purchase_stock',
    ],
    'data': [
        'views/res_config_setting.xml',
        'views/res_company.xml'
    ],
    'installable': True,
}