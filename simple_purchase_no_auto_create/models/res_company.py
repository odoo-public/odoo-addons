from odoo import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    auto_create_supplierinfo = fields.Selection(
        string='Auto Create Supplier Info Method',
        selection=[
            ('no', 'No '),
            ('auto', 'Auto Create'),
            ('auto_parent', 'Auto Create Parent - When Contact used'),
        ],
        default='auto',
        help="When creating supllier orders get more control")