from odoo import fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    auto_create_supplierinfo = fields.Selection(
        related='company_id.auto_create_supplierinfo',
        readonly=False,
        default='auto')