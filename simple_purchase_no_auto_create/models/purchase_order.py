# -*- coding: utf-8 -*-
# Copyright© 2016 ICTSTUDIO <http://www.ictstudio.eu>
# License: LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)


import logging
from odoo import api, models, tools
from odoo.exceptions import AccessError, UserError, ValidationError

_logger = logging.getLogger(__name__)


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def _add_supplier_to_product(self):
        product_list = []
        for line in self.order_line:
            if line.order_id.company_id.auto_create_supplierinfo == 'auto_parent':
                partner = self.partner_id if not self.partner_id.parent_id else self.partner_id.parent_id
            else:
                partner = self.partner_id
            if line.product_id and partner not in line.product_id.seller_ids.mapped('name') and len(line.product_id.seller_ids) <= 10:
                if line.order_id.company_id.auto_create_supplierinfo in ['auto', 'auto_parent']:
                    return super(PurchaseOrder, self)._add_supplier_to_product()
                else:
                    product_list.append(line.product_id.name)

        if product_list:
            product_names = ', '.join(product_list)
            raise UserError("Please make sure "+"["+product_names+"]"+" has vendor "+"["+self.partner_id.name+"]")
