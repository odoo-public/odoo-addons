# -*- coding: utf-8 -*-
# Copyright© 2021 ICTSTUDIO <http://www.ictstudio.eu>
# See LICENSE file for full copyright and licensing details.

import logging
from odoo import models, fields, api, _


_logger = logging.getLogger(__name__)

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def open_product_stock_warehouse(self):
        self.ensure_one()
        wizard = self.env['product.stock.warehouse'].create({
            'product_template_id': self.id
        })

        return {
            'name': _('Product Warehouse Stock'),
            'view_mode': 'form',
            'res_model': 'product.stock.warehouse',
            'views': [(self.env.ref('product_stock_warehouse.product_stock_warehouse_view_form').id, 'form')],
            'type': 'ir.actions.act_window',
            'res_id': wizard.id,
            'target': 'new',
            'context': self.env.context,
        }