# -*- coding: utf-8 -*-
# Copyright© 2021 ICTSTUDIO <http://www.ictstudio.eu>
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)

class ProductProduct(models.Model):
    _inherit = 'product.product'

    def open_product_stock_warehouse(self):
        self.ensure_one()
        wizard = self.env['product.stock.warehouse'].create({
            'product_template_id': self.product_tmpl_id.id, 'product_variant_id': self.id
        })

        return {
            'name': _('Product Warehouse Stock'),
            'view_mode': 'tree',
            'res_model': 'product.stock.warehouse.line',
            'views': [(self.env.ref('product_stock_warehouse.product_stock_warehouse_view_form').id, 'form')],
            'type': 'ir.actions.act_window',
            'res_id': wizard.id,
            'target': 'new',
            'context': self.env.context,
        }