# -*- coding: utf-8 -*-
# Copyright© 2021 ICTSTUDIO <http://www.ictstudio.eu>
# See LICENSE file for full copyright and licensing details.
{
    'name': 'Products Stock Warehouse',
    'version': '14.0.0.0.1',
    'author': 'ICTSTUDIO, André Schenkels',
    'license': 'OPL-1',
    'website': 'http://www.ictstudio.eu',
    'complexity': 'normal',
    'summary': "Show Stock Available each Warehouse",
    'category': 'Stock Management',
    'depends': [
        'stock'
    ],
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/product_template.xml',
        'wizard/product_stock_warehouse.xml',
        'wizard/product_stock_warehouse_line.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'price': '235',
    'currency': 'EUR'
}

