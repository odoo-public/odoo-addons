# -*- coding: utf-8 -*-
# Copyright© 2021 ICTSTUDIO <http://www.ictstudio.eu>
# See LICENSE file for full copyright and licensing details.

import logging
from odoo import models, fields, api, _

_logger = logging.getLogger(__name__)

class ProductStockWarehouse(models.TransientModel):
    _name = 'product.stock.warehouse'
    _description = 'Transient model showing the Stock differently'

    product_template_id = fields.Many2one(comodel_name='product.template')
    product_variant_id = fields.Many2one(comodel_name='product.product')
    product_attributes = fields.One2many(comodel_name='product.attribute', compute='_get_product_attributes')
    select_attribute_id = fields.Many2one(
        comodel_name='product.attribute'
    )
    select_attribute_value = fields.Many2one(comodel_name='product.attribute.value')
    group_by_attribute_id = fields.Many2one(
        comodel_name='product.attribute'
    )
    stock_warehouse_line_ids = fields.One2many('product.stock.warehouse.line', 'product_stock_warehouse_id', compute='_get_lines')
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, default=lambda self: self.env.company)

    @api.depends('product_template_id')
    def _get_product_attributes(self):
        for rec in self:
            rec.product_attributes = self.product_template_id.attribute_line_ids.mapped('attribute_id')

    @api.onchange('product_template_id','select_attribute_id','group_by_attribute_id')
    def onchange_product_template_id(self):
        
        if self.product_template_id.attribute_line_ids.mapped('attribute_id'):
            domain = {
                'select_attribute_id': [('id', 'in', self.product_template_id.attribute_line_ids.mapped('attribute_id').ids)],
                'select_attribute_value': [('id', 'in', self.product_template_id.attribute_line_ids.mapped('attribute_id').value_ids.ids)],
                'group_by_attribute_id': [('id', 'in', self.product_template_id.attribute_line_ids.mapped('attribute_id').ids)]
            }
            if self.select_attribute_id:
                domain.update({
                    'select_attribute_value': [('id', 'in', self.select_attribute_id.value_ids.ids)],
                })
        else:
            domain = {
                'select_attribute_id': [('id', 'in', [])],
                'select_attribute_value': [('id', 'in', [])],
                'group_by_attribute_id': [('id', 'in', [])]
            }
        return {'domain': domain}

    # @api.onchange('select_attribute_id')
    # def onchange_select_attribute_id(self):
    #     if self.select_attribute_id:
    #         domain = {
    #             'select_attribute_value': [('id', 'in', self.select_attribute_id.value_ids.ids)],
    #         }
    #     else:
    #         domain = {
    #             'select_attribute_value': [('id', 'in', [])]
    #         }
    #     return {'domain': domain}

    def _get_product_attribute_value(self, product, attribute):
        templ_value_id = product.product_template_attribute_value_ids.filtered(lambda f: f.attribute_id == attribute)
        if templ_value_id:
            return templ_value_id.product_attribute_value_id
        return False

    def _get_warehouse_stock(self, product, warehouses):
        wline = self.env['product.stock.warehouse.line']
        for warehouse in warehouses:
            stock_product = product.with_context(warehouse=warehouse.id)
            select_attr_value = self._get_product_attribute_value(product, self.select_attribute_id)
            group_attr_value = self._get_product_attribute_value(product, self.group_by_attribute_id)
            if group_attr_value and select_attr_value == self.select_attribute_value:
                wline |= wline.create({
                    'product_stock_warehouse_id': self.id,
                    'select_attribute_id': self.select_attribute_id.id,
                    'select_attribute_value': self.select_attribute_value.id,
                    'group_by_attribute_id': self.group_by_attribute_id.id,
                    'group_by_attribute_value':group_attr_value.id,
                    'warehouse_id': warehouse.id, 
                    'location_id': warehouse.view_location_id.id,
                    'qty_available': stock_product.qty_available,
                    'incoming_qty': stock_product.incoming_qty,
                    'outgoing_qty': stock_product.outgoing_qty,
                    'virtual_available': stock_product.virtual_available,
                    'free_qty': stock_product.free_qty,
                    'product_id': product.id
                    })
        return wline

    @api.onchange('product_template_id','select_attribute_id', 'group_by_attribute_id', 'select_attribute_value')
    def _get_lines(self):
        for rec in self:
            wlines = self.env['product.stock.warehouse.line']
            if self.select_attribute_id and self.group_by_attribute_id and self.select_attribute_value:
                warehouses = self.env['stock.warehouse'].search([('company_id', '=', self.company_id.id)])
                for product in self.product_template_id.product_variant_ids:
                    wlines |= self._get_warehouse_stock(product, warehouses)
            rec.stock_warehouse_line_ids = wlines

    def open_lines(self):
        self.ensure_one()

        return {
            'name': _('Product Warehouse Stock Lines'),
            'view_mode': 'form',
            'res_model': 'product.stock.warehouse.line',
            'views': [
                (self.env.ref('product_stock_warehouse.product_stock_warehouse_line_view_tree').id, 'tree'), 
                (self.env.ref('product_stock_warehouse.product_stock_warehouse_line_view_search').id, 'search')
                ],
            'type': 'ir.actions.act_window',
            'domain': [('product_stock_warehouse_id', '=', self.id)],
            'target': 'new',
            'context': self.env.context,
        }


class ProductStockWarehouseLine(models.TransientModel):
    _name = 'product.stock.warehouse.line'
    _description = 'Transient Line'
    _order = 'group_by_attribute_value, warehouse_id'

    product_stock_warehouse_id = fields.Many2one(comodel_name='product.stock.warehouse')
    product_template_id = fields.Many2one(related='product_stock_warehouse_id.product_template_id')
    product_id = fields.Many2one(comodel_name='product.product')
    warehouse_id = fields.Many2one('stock.warehouse')
    location_id = fields.Many2one('stock.location')
    select_attribute_id = fields.Many2one(comodel_name='product.attribute')
    select_attribute_value = fields.Many2one(comodel_name='product.attribute.value')
    group_by_attribute_id = fields.Many2one(comodel_name='product.attribute')
    group_by_attribute_value = fields.Many2one(comodel_name='product.attribute.value')
    qty_available = fields.Float()
    incoming_qty = fields.Float()
    outgoing_qty = fields.Float()
    virtual_available = fields.Float()
    free_qty = fields.Float()