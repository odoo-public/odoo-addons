.. image:: https://img.shields.io/badge/License-LGPL%20v3-blue.svg
    :alt: License: LGPL-3

Dropshipment Client Order Reference
===================================
Add client order ref to the dropship address

Bug Tracker
===========
Bugs are tracked on `Gitlab Issues <https://gitlab.com/odoo-public/odoo-addons/-/issues>`_.

Maintainer
==========
.. image:: https://www.ictstudio.eu/github_logo.png
   :alt: ICTSTUDIO
   :target: https://www.ictstudio.eu

This module is maintained by the ICTSTUDIO.