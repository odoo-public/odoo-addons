# -*- coding: utf-8 -*-
# Copyright© 2017 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

from odoo import models, fields, api

class ResPartnerContactRelation(models.Model):
    _name = 'res.partner.contact.relation'

    name = fields.Char(string="Relation")
