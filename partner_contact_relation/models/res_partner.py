# -*- coding: utf-8 -*-
# Copyright© 2017 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

import re
from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    contact_relation = fields.Many2one(
        comodel_name='res.partner.contact.relation',
        string="Relation")
