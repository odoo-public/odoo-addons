# -*- coding: utf-8 -*-
# Copyright© 2016-2017 ICTSTUDIO <http://www.ictstudio.eu>
# License: LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)
{
    'name': 'Partner Contact Relation',
    'version': '12.0.0.0.1',
    'category': 'Contact Management',
    'author': 'ICTSTUDIO | André Schenkels',
    'website': 'http://www.ictstudio.eu',
    'license': 'LGPL-3',
    'summary': 'When main partner is a person, add relation to contact',
    'depends': [
        'base',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner_contact_relation.xml',
        'views/res_partner.xml',
    ],
    'installable': True,
}