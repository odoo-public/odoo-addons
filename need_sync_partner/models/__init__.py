# Copyright© 2022 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

from . import need_sync_model
from . import res_partner
from . import need_sync_line
