# -*- coding: utf-8 -*-
# Copyright© 2017-today ICTSTUDIO <http://www.ictstudio.eu>
# License: LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from odoo import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)

class ProductTemplate(models.Model):
    _inherit = "product.template"

    external_stock_qty = fields.Float(
        compute='compute_external_stock_qty',
        string="External Stock Qty",
        store=True)

    def action_view_external_stock(self):
        self.ensure_one()
        action = {"type": "ir.actions.act_window_close"}
        action = self.env["ir.actions.actions"]._for_xml_id("product_external_stock.action_product_external_stock")
        action["context"] = {
                "search_default_product_tmpl_id": self.id}
        return action

    @api.depends('seller_ids', 'seller_ids.external_stock_qty')
    def compute_external_stock_qty(self):
        for rec in self:
            rec.external_stock_qty = sum(
                rec.mapped('seller_ids.external_stock_qty')
            )
