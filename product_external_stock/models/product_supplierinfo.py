# -*- coding: utf-8 -*-
# Copyright© 2017- erp-m <http://www.erp-m.eu>
import logging

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError

import odoo.addons.decimal_precision as dp
_logger = logging.getLogger(__name__)

class SupplierInfo(models.Model):
    _inherit = "product.supplierinfo"

    product_external_stock = fields.One2many(
        comodel_name='product.external.stock',
        inverse_name="supplierinfo"
    )

    external_stock_qty = fields.Float(compute='compute_external_stock', store=True)
    external_stock_datetime = fields.Datetime(compute='compute_external_stock', store=True)

    @api.depends('product_external_stock', 'product_external_stock.quantity', 'product_external_stock.supplierinfo')
    def compute_external_stock(self):
        for rec in self:
            if rec.product_external_stock:
                rec.external_stock_qty = rec.product_external_stock[0].quantity
                rec.external_stock_datetime = rec.product_external_stock[0].datetime
