# -*- coding: utf-8 -*-
# Copyright© 2017-today ICTSTUDIO <http://www.ictstudio.eu>
# License: LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from odoo import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)

class ProductExternalStock(models.Model):
    _name = "product.external.stock"
    _description = "External Stock (Dropshipping)"
    _order = 'datetime desc'

    supplierinfo = fields.Many2one(
        comodel_name='product.supplierinfo',

    )
    product_id = fields.Many2one(related='supplierinfo.product_id', store=True)
    product_tmpl_id = fields.Many2one(related='supplierinfo.product_tmpl_id', store=True)
    quantity = fields.Float()
    datetime = fields.Datetime(default=fields.Datetime.now)
    active = fields.Boolean(default=True)
