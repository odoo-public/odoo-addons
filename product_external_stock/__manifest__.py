# -*- coding: utf-8 -*-
# Copyright© 2017-today ICTSTUDIO <http://www.ictstudio.eu>
# License: LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)
{
    "name": "Product External Stock",
    "version": "16.0.1.0.5",
    "category": "Stock",
    "author": "ICTSTUDIO, André Schenkels",
    "website": "http://www.ictstudio.eu",
    "license": "LGPL-3",
    "summary": "Manage External Stock",
    "depends": [
        "sale_stock",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/product_external_stock.xml",
        "views/product_template.xml",
        "views/product_supplierinfo.xml",
    ],
}
