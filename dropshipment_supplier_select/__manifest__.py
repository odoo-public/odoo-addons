# -*- coding: utf-8 -*-
# Copyright© 2016-2017 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)
{
    'name': 'Dropshipment Supplier Select',
    'version': '16.0.0.0.1',
    'category': 'Stock',
    'author': 'ICTSTUDIO | André Schenkels',
    'website': 'http://www.ictstudio.eu',
    'license': 'LGPL-3',
    'summary': 'Provide a way to select the designated supplier on a sale order line.',
    'depends': [
        'stock_dropshipping',
        'purchase'
    ],
    'data': [
        'views/sale_order.xml',
    ],
}