.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :alt: License: AGPL-3

Dropshipment Supplier Select
============================
Provide a way to select the designated supplier on a sale order line.

Bug Tracker
===========
Bugs are tracked on `Gitlab Issues <https://gitlab.com/odoo-public/odoo-addons/-/issues>`_.

Maintainer
==========
.. image:: https://www.ictstudio.eu/github_logo.png
   :alt: ICTSTUDIO
   :target: https://www.ictstudio.eu

This module is maintained by the ICTSTUDIO.