# -*- coding: utf-8 -*-
# Copyright© 2016-2017 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

import urllib
import logging
from odoo import models, fields, api, _
from odoo.exceptions import except_orm, Warning, RedirectWarning

_logger = logging.getLogger(__name__)


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    supplier_id = fields.Many2one(
        comodel_name='product.supplierinfo',
        string='Supplier')
    route_enabled = fields.Boolean(
        string='Route Enabled',
        default=False)

    @api.onchange('route_id')
    def _onchange_route_id(self):
        if self.route_id:
            self.route_enabled = True

    def _prepare_procurement_values(self, group_id=False):
        """ Prepare specific key for moves or other components that will be created from a stock rule
        coming from a sale order line. This method could be override in order to add other custom key that could
        be used in move/po creation.
        """
        values = super(SaleOrderLine, self)._prepare_procurement_values(group_id)
        values.update({
            'supplier_id': self.supplier_id,
            'supplierinfo_id': self.supplier_id
        })
        return values