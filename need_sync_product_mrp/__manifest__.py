# Copyright© 2022 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)
{
    "name": "Need Synchronization Product MRP Module",
    "version": "16.0.0.0.7",
    "category": "Stock",
    "author": "ICTSTUDIO, André Schenkels",
    "website": "http://www.ictstudio.eu",
    "license": "AGPL-3",
    "summary": "MRP Specific module for seting need sync",
    "depends": ["need_sync_product", "mrp"],
    "data": [],
    "installable": True,
}
