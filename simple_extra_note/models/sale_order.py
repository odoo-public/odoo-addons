# Copyright© 2020 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

import logging

from odoo import models, fields

_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _inherit = "sale.order"

    extra_note_copy_to_picking = fields.Boolean(
        string="Copy to Delivery",
        help="Copy the Extra Note(s) to the Pickings and Move Lines.",
        default=True)
    extra_note_copy_to_invoice = fields.Boolean(
        string="Copy to Invoice",
        help="Copy the Extra Note(s) to the Invoice.",
        default=True)
    extra_note = fields.Text(
        string="Note")

    def _prepare_invoice(self):
        values = super(SaleOrder, self)._prepare_invoice()
        if values and self.extra_note_copy_to_invoice and self.extra_note:
            values.update({'extra_note': self.extra_note})
        return values
