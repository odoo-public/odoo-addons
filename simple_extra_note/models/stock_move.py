# Copyright© 2020 ICTSTUDIO <http://www.ictstudio.eu>
# License: AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

import logging

from odoo import models, fields

_logger = logging.getLogger(__name__)

class StockMove(models.Model):
    _inherit = "stock.move"

    def _get_new_picking_values(self):
        """ return create values for new picking that will be linked with group
        of moves in self.
        """
        values = super(StockMove, self)._get_new_picking_values()
        order = self.mapped('group_id').sale_id
        if order and values and order.extra_note_copy_to_picking:
            values.update({'extra_note': order.extra_note})
        return values
